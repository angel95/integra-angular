import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  public faqs = [
    {
      title: '¿Lorem ipsum dolor sit amet?',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a justo nec tortor tempus molestie. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
      rotate: false,
    },
    {
      title: '¿Lorem ipsum dolor sit amet?',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a justo nec tortor tempus molestie. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
      rotate: false,
    },
    {
      title: '¿Lorem ipsum dolor sit amet?',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a justo nec tortor tempus molestie. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
      rotate: false,
    },
    {
      title: '¿Lorem ipsum dolor sit amet?',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a justo nec tortor tempus molestie. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
      rotate: false,
    },
    {
      title: '¿Lorem ipsum dolor sit amet?',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a justo nec tortor tempus molestie. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
      rotate: false,
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

  rotateEl(index: number): void{
    const actualValue = this.faqs[index].rotate;
    this.faqs.forEach(faq => faq.rotate = false);
    if(actualValue){
      this.faqs[index].rotate = false
    } else{
      this.faqs[index].rotate = true
    }
  }


}
