import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comentarios',
  templateUrl: './comentarios.component.html',
  styleUrls: ['./comentarios.component.scss']
})
export class ComentariosComponent implements OnInit {
  public comentarios = [
    {
      title: 'Comentario',
      avatar: 'http://placehold.it/32x32',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a justo nec tortor tempus molestie.',
      like: -1,
    },
    {
      title: 'Comentario',
      avatar: 'http://placehold.it/32x32',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a justo nec tortor tempus molestie.',
      like: -1,
    },
    {
      title: 'Comentario',
      avatar: 'http://placehold.it/32x32',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a justo nec tortor tempus molestie.',
      like: -1,
    },
    {
      title: 'Comentario',
      avatar: 'http://placehold.it/32x32',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a justo nec tortor tempus molestie.',
      like: -1,
    },
    {
      title: 'Comentario',
      avatar: 'http://placehold.it/32x32',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a justo nec tortor tempus molestie.',
      like: -1,
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
