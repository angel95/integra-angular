import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComentariosComponent } from './comentarios/comentarios.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { FaqComponent } from './faq/faq.component';
import { MainCardComponent } from './main-card/main-card.component';
import { LayoutComponent } from './layout/layout.component';



@NgModule({
  declarations: [
    ComentariosComponent,
    NavbarComponent,
    FooterComponent,
    FaqComponent,
    MainCardComponent,
    LayoutComponent
  ],
  imports: [
    CommonModule
  ], exports: [
    LayoutComponent,
    NavbarComponent,
    FooterComponent
  ]
})
export class LandingModule { }
