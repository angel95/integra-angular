import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-card',
  templateUrl: './main-card.component.html',
  styleUrls: ['./main-card.component.scss']
})
export class MainCardComponent implements OnInit {
  public seeMore: boolean = false;
  public hideText: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }


  effectTextHelper(){
    this.hideText = true;
    //timeouts para el efecto del vermas
    setTimeout(() => {
      this.seeMore = !this.seeMore
    }, 10);
    setTimeout(() => {
      this.hideText = false;
    }, this.seeMore ? 350 : 50);
  }
}
